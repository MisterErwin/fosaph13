# Aufgabe 13 - first longest match

## Selber bauen

Das Projekt nutzt maven (für __graphviz-java__, __logback-classic__und __guava__)

## Automat Erstellen

Sei **R** eine Menge regulärer Ausdrücke.

Sei **Mi** der (per Thomsonkonstruktion erzeugte) (endliche) Automat, welcher Ri erkennt.
 
Sei nun **DM** der NFA, welcher durch das Zusammensetzen aller _Mi_ s erstellt wurde.

![Das Konzept](out/konzept.svg)

Und **M** der DFA, welcher per Potenzmengenkonstruktion aus _DM_ erzeugt wurde.

Für die besagte Sprache (aus Tut 17) ergibt dich folgender NFA:
[DM: ![Anfangsdfa](out/nfa.svg)](out/nfa.svg)

Bzw der DFA: 
[M: ![Anfangsdfa](out/dfa.svg)](out/dfa.svg)



## First Longest Match

Sei _simulatePathsToEnd_ eine Funktion, welche für ein Wort alle Endzustände ermittelt, 
in denen das Wort (oder eine kürzere Version/Ende abgetrennt) endet.

Das Ergebnis der Funktion ist eine Menge von Tupeln `{([benutzes Teilwort],Endzustand), ...} `

Gesucht ist jetzt der Endzustand, dessen Ausdruck am weitesten "vorne" in R steht.
Das benutzte Teilwort wird jetzt abgeschnitten, und das Ganze so lange wiedeholt bist das Wort leer ist, 
oder M nichts per findet(=Sprache nicht erkannt).

## M minimieren
Für eine bessere Geschwindigkeit, müsste __M__ minimiert werden.

Eine "einfache" Methode ist der Brzozowski-Algorithmus.
DFA reversieren -> NFA zu DFA -> DFA reversieren -> NFA zu DFA.
Während hier ein minimierter DFA entsteht, welcher die Wörter erkennt,
gehen bei der Minimierung Informationen verloren, die diese Implementierung von FLM braucht.

Insbesonders werden hier die Endzustände verschmolzen, wodurch am Ende nicht mehr ersichtlich ist,
welcher der Ausdrücke benutzt wurde.

Eine exemplarische Minimierung (Die Zoom-Funktion ist zu Empfehlen):

[Anfangsdfa: ![Anfangsdfa](out/dfa.svg)](out/dfa.svg)

[reverse 1: ![Anfangsdfa](out/dfarev.svg)](out/dfarev.svg)

[zu DFA: ![Anfangsdfa](out/dfarevdfa.svg)](out/dfarevdfa.svg)

[reverse 2: ![Anfangsdfa](out/dfarevdfarev.svg)](out/dfarevdfarev.svg)

[finales zu DFA: ![Anfangsdfa](out/dfarevdfarevdfa.svg)](out/dfarevdfarevdfa.svg)

Bei der Potenzmengenkonstruktion werden die Endzustände verschmolzen.

## Ausgabe

```
##################
flm auf <Wort>
<symbolischer-Name>=><u_i>
...
------------------
Wort komplett durchlaufen (oder) Wort unvollständig durchlaufen
##################
```
Wobei jede Zeile einen symbolischen Namen und das dazu gehörige u_i darstellt.

(Diverse Ausgaben stehen in der _out.txt_ Datei)

## Diverses
Jedes Bild existiert als _name.svg_, _nameb.png_ und _names.png_ im Ordner out (Außnahme _dfarevdfarevdfa_)