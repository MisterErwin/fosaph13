package fosap;

import guru.nidi.graphviz.attribute.Shape;
import guru.nidi.graphviz.attribute.Style;
import guru.nidi.graphviz.engine.Format;
import guru.nidi.graphviz.engine.Graphviz;
import guru.nidi.graphviz.engine.Rasterizer;
import guru.nidi.graphviz.model.Label;

import java.io.File;

import static guru.nidi.graphviz.model.Factory.*;

/**
 * Created by Alex Lüpges on 25.06.2017.
 * In case you need it, ask me ;)
 *
 * Just to generate the konzept graphs
 */
public class Desc {
    public static void main(String[] a) throws Exception {

        Graphviz viz = Graphviz.fromGraph( graph().directed().with(
                node("realstart").with(Style.INVIS).link(
                        node("").link(
                                to(node("M1").link(to(node(" ").with(Shape.DOUBLE_CIRCLE)).with(Label.of("ep")))).with(Label.of("ep")),
                                to(node("M2").link(to(node(" ").with(Shape.DOUBLE_CIRCLE)).with(Label.of("ep")))).with(Label.of("ep")),
                                to(node("M3").link(to(node(" ").with(Shape.DOUBLE_CIRCLE)).with(Label.of("ep")))).with(Label.of("ep"))
                        )
                ))
        );
        String filename = "out/konzept";
        viz.width(200).render(Format.SVG).toFile(new File(filename + ".svg"));
        viz.width(200).rasterizer(Rasterizer.BATIK).render(Format.PNG).toFile(new File(filename + "b.png"));
        viz.width(200).rasterizer(Rasterizer.SALAMANDER).render(Format.PNG).toFile(new File(filename + "s.png"));

    }
}
