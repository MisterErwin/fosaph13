package fosap;

import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.primitives.Chars;

import java.util.*;

/**
 * Created by Alexander Lüpges on 19.06.2017.
 * In case you need it, ask me ;)
 */
@SuppressWarnings("WeakerAccess")
public class Auf13 {
    private String[] R;


    Auf13() throws Exception {
        //START Aufbau NFA
        Automat<String, Character> M = new Automat<>("START");


        // "Wörter", die erkannt werden
        R = new String[]{"for", "while", "if", ";", "<", "++", "+", "(", ")", "{", "}", "="};
        Arrays.sort(R, Comparator.comparingInt(String::length));

        int i = 0;
        for (String r : R) {
            //Mache einen Automaten aus jedem Wort (mit einem Index um die Zustände Einzigartig zu halten)
            MData mData = makeM(r, Integer.toString(i));
            i++;
            M.addTransition("START", M.EPSILON, mData.M); //Transition Start -ep-> WortStart
            M.addTransition(mData.last, M.EPSILON, "END" + r); //Zwischen Endzustand (für die Erkennung am Ende)
            M.addTransition("END" + r, M.EPSILON, "END"); //Vom Zwischenzustand -ep-> gemeinsamer Endzustand
        }
        //rNum hinzufügen
        M.addTransition("START", M.EPSILON, rNum());
        M.addTransition("ENDnum", M.EPSILON, "END");
        //rID hinzufpgen
        M.addTransition("START", M.EPSILON, rID());
        M.addTransition("ENDid", M.EPSILON, "END");
        M.addEnd("END");


        //Hier könnte man den NFA optimieren.
        /*
        Hier könnte man den NFA optimieren.
        Ein Ansatz wäre nach Brzozowski gehen, also zum DFA -> Umdrehen -> zum DFA -> Umdrehen -> zum DFA.
        Hierbei würden aber die Enden "verschmelzen", d.h. in der check Methode wäre der Pfad nicht unterscheidbar.
         */


//        M.toImage("out/nfa");
        M = M.toDFA();
//        M.toImage("out/dfa");

        //Graphen für die Dokumentation. OutOfMemoryErrors können passieren für pngs und eine heap size kleiner als 8 GB
        /*
        Automat<String,Character> MM = M.reverse();
        MM.toImage("out/dfarev");
        MM = MM.toDFA();
        System.gc();
        MM.toImage("out/dfarevdfa");
        MM = MM.reverse();
        System.gc();
        MM.toImage("out/dfarevdfarev");
        MM = MM.toDFA();
        MM.toImage("out/dfarevdfarevdfa");
        */
        //ENDE Aufbau NFA

        flmPrint(M, "for(i=0;i<10;i++){print(i);}");
        flmPrint(M, "i=0;while(i++<23){if(i=12){print(i);}}");
        flmPrint(M, "if(1!=0)");
        flmPrint(M, "Hier könnte ihre Programmiersprache stehen");
        flmPrint(M, "a+long+time+ago;+in+a+galaxy+far");
    }

    /**
     * Ruft die Checkmethode auf, und gibt deren return Map aus.
     *
     * @param M Automat
     * @param w String, der geprüft werden soll
     */
    void flmPrint(Automat<String, Character> M, String w) {
        List<Map.Entry<String, String>> ret = new LinkedList<>();
        boolean isAtEnd = flm(Chars.asList(w.toCharArray()), ret, M);
        System.out.println(Strings.repeat("#", w.length()+10));
        System.out.println("flm auf " + w);
        for (Map.Entry<String, String> e : ret)
            System.out.println(e.getKey() + "=>" + e.getValue());
        System.out.println(Strings.repeat("-", w.length()+10));
        if (isAtEnd)
            System.out.println("Wort komplett durchlaufen");
        else
            System.out.println("Wort unvollständig durchlaufen");
        System.out.println(Strings.repeat("#", w.length()+10));
    }


    /**
     * Versucht first longest match auf einem DFA anzuwenden
     *
     * @param w       Liste an Chars, die überprüft werden soll
     * @param realRet eine Liste, in welcher die Tokens stehen
     * @param DFA     ein Automat (als DFA!)
     * @return ob das Ende des Wortes w erreicht wurde
     */
    boolean flm(List<Character> w, List<Map.Entry<String, String>> realRet, Automat<String, Character> DFA) {
        Map<List<Character>, String> map = new HashMap<>();
        //Welche "Wege" können wir mit w (oder Anfangsteilen von w) laufen, so dass wir in einem Endzustand landen
        DFA.simulatePathsToEnd(DFA.getStart(), w, new ArrayList<>(), map);
        //Sortiere map nach Vorgabe
        TreeMap<List<Character>, String> sorted = new TreeMap<>((o1, o2) -> {
            //Je größer der Index, desto länger das Prefix (Aussnahme rnum und rid)
            int i1 = Integer.MIN_VALUE;
            String x = map.get(o1);
            for (String s : x.substring(1, x.length() - 1).split(","))
                i1 = Math.max(i1, getEndIndex(s));
            int i2 = Integer.MIN_VALUE;
            x = map.get(o2);
            for (String s : x.substring(1, x.length() - 1).split(","))
                i2 = Math.max(i2, getEndIndex(s));
            if (i2 == i1) //Falls beide Prefixe gleichwertig sind
                return Integer.compare(o2.size(), o1.size()); //Längeres Wort hat hier den Vorzug
            return Integer.compare(i2, i1); //Längeres Prefix gewinnt
        });
        sorted.putAll(map);
//        System.out.println("SORTED: " + sorted.toString()); // Test-Ausgabe
        if (sorted.size() > 0) { //Es wurde etwas gefunden
            //First entry = longest Prefix (Ausnahme rID/rNUM)
            //Füge (symbolischer Name, Teile von w, die benutzt wurden) als Token hinzu
            realRet.add(new AbstractMap.SimpleEntry<>(extractName(sorted.firstEntry().getValue()), Joiner.on("").join(sorted.firstKey())));
//            System.out.println("next check" + w.subList(sorted.firstKey().size(), w.size()).toString()); //Test-Ausgabe
            //Auf den Rest des Worted flm anwenden (und den Rückgabewert von fml durchreichen)
            return flm(w.subList(sorted.firstKey().size(), w.size()), realRet, DFA);
        } else { //Am Ende angelangt
//            System.out.println("--end of the show--"); //Test-Ausgabe
            return w.isEmpty();
        }

    }

    /**
     * Private helper function, returning the weight of a node for sorting
     *
     * @param o Node name
     * @return the weight of o
     */
    private int getEndIndex(String o) {
        for (int i = 0; i < R.length; ++i)
            if (("END" + R[i]).equals(o))
                return i;
        if ("ENDnum".equals(o)) //rNUm am Ende
            return -100;
        if ("ENDid".equals(o)) //Aber rID als letztes
            return -200;
        return Integer.MIN_VALUE;
    }

    /**
     * Strips node name information to return a symbolic name (e.g. for, id, ...)
     *
     * @param x the node name
     * @return the symbolic name
     */
    private String extractName(String x) {
        if (x.startsWith("[") && x.endsWith("]")) //Strip Potenzmengenkonstr
            x = x.substring(1, x.length() - 1);
        List<String> ends = new ArrayList<>();
        ends.addAll(Arrays.asList(x.split(",")));
        //Wähle Name nach Gewichtung
        ends.sort((o1, o2) -> Integer.compare(getEndIndex(o2), getEndIndex(o1)));

        return ends.get(0).substring(3);
    }


    /**
     * Methode um den reg. Ausdruck für rNum als Automat zurückzugeben
     *
     * @return the Automat
     */
    private Automat<String, Character> rNum() {
        Automat<String, Character> Automat = new Automat<>("numStart");
        for (int i = 0; i <= 9; ++i)
            Automat.addTransition("numStart", Character.forDigit(i, 10), "ENDnum");
        for (int i = 0; i <= 9; ++i)
            Automat.addTransition("ENDnum", null, "numStart");
        Automat.addEnd("ENDnum");
        return Automat;
    }

    /**
     * Methode um den reg. Ausdruck für rID als Automat zurückzugeben
     *
     * @return the Automat
     */
    private Automat<String, Character> rID() {
        Automat<String, Character> Automat = new Automat<>("idStart");
        for (int i = 0; i < 26; i++)
            Automat.addTransition("idStart", (char) ('a' + i), "ENDid");
        for (int i = 0; i < 26; i++)
            Automat.addTransition("ENDid", (char) ('a' + i), "ENDid");
        for (int i = 0; i <= 9; ++i)
            Automat.addTransition("ENDid", Character.forDigit(i, 10), "ENDid");
        Automat.addTransition("ENDid", null, "ENDid");
        Automat.addEnd("ENDid");
        return Automat;
    }

    /**
     * Methode um einen Automaten für einen token zu generieren
     *
     * @param token Wort, welches zu einem endlichen Automaten wird
     * @param index sorgt für eindeutige Node namen11
     * @return the Automat
     */
    private MData makeM(String token, String index) {
        String state = "##" + index + "Start";

        Automat<String, Character> M = new Automat<>(state);
        int i = 0;
        while (i < token.length()) {
            String n = token.charAt(i) + "##" + index + " " + i;
            M.addTransition(state, token.charAt(i), n);
            state = n;
            i++;
        }
        return new MData(M, state);
    }

    public static void main(String[] a) throws Exception {
        new Auf13();
    }

    /**
     * Data class for FA generation
     */
    private class MData {
        private Automat<String, Character> M;
        private String last;

        MData(Automat<String, Character> m, String last) {
            M = m;
            this.last = last;
        }
    }
}
