package fosap;

import com.google.common.base.Joiner;
import guru.nidi.graphviz.attribute.Arrow;
import guru.nidi.graphviz.attribute.Shape;
import guru.nidi.graphviz.attribute.Style;
import guru.nidi.graphviz.engine.Format;
import guru.nidi.graphviz.engine.Graphviz;
import guru.nidi.graphviz.engine.Rasterizer;
import guru.nidi.graphviz.model.*;

import java.io.File;
import java.io.IOException;
import java.util.*;

import static guru.nidi.graphviz.model.Factory.*;

@SuppressWarnings("WeakerAccess,unused")
public class Automat<S, A> {

    private final Map<S, Map<A, List<S>>> data;
    private final S start;
    private final Set<S> ends;

    public A EPSILON = null; //Hier sind Epsilons echte null Kanten :)


    public Automat(S start) {
        data = new HashMap<>();
        this.start = start;
        this.ends = new HashSet<>();
        this.data.put(start, new HashMap<>());
    }

    /**
     * Adds a transition from q to p on the symbol a
     *
     * @param q State to go from
     * @param a Symbol
     * @param p target.state
     * @return if a change of the data happened
     */
    public boolean addTransition(S q, A a, S p) {
        if (!data.containsKey(q))
            data.put(q, new HashMap<>());
        if (!data.containsKey(p))
            data.put(p, new HashMap<>());

        List<S> list = data.get(q).computeIfAbsent(a, k -> new ArrayList<>());
        boolean b = list.contains(p);
        if (!b)
            list.add(p);
        return !b;
    }

    public void addTransition(S q, A a, Automat<S, A> nextNFA) {
        if (!data.containsKey(q))
            data.put(q, new HashMap<>());

        data.putAll(nextNFA.data);
        addTransition(q, a, nextNFA.start);
    }

    public void addEnd(S q) {
        ends.add(q);
    }


    /**
     * Generates a graphviz Graph
     *
     * @return a MutableGraph representing this finite automata
     */
    private MutableGraph toGraph() {
        MutableGraph graph = mutGraph("name").setDirected();

        Node startNode = node("realstart" + hashCode()).with(Style.INVIS);

        Map<S, MutableNode> nodes = new HashMap<>();
        for (S s : data.keySet()) {
            MutableNode n = mutNode(s.toString());//.split("##")[0]);
            if (ends.contains(s))
                n.add(Shape.DOUBLE_CIRCLE);
            graph.add(n);
            nodes.put(s, n);
        }
        for (Map.Entry<S, Map<A, List<S>>> e : data.entrySet()) {
            MutableNode n = nodes.get(e.getKey());
            for (A symbol : e.getValue().keySet()) {
                String symbolName = symbol == null ? "ep" : symbol.toString();
                for (S nextState : e.getValue().get(symbol)) {
                    MutableNode nextNode = nodes.get(nextState);
                    if (e.getKey().toString().compareTo(nextState.toString()) >= 0) {
                        nextNode.addLink(to(n.withRecord(" ")).with(Label.of(symbolName), Arrow.INV.open()));

                    } else {
                        n.addLink(to(nextNode.withRecord(" ")).with(Label.of(symbolName)));
                    }
                }
            }
        }
        if (this.start != null) {
            graph.add(startNode.link(nodes.get(this.start)));
        }
        return graph;

    }

    /**
     * Generates some image files
     *
     * @param filename the filename (must be in a sub folder)
     * @throws IOException welp
     */
    public void toImage(String filename) throws IOException {
        Graphviz viz = Graphviz.fromGraph(toGraph());
        viz.render(Format.SVG).toFile(new File(filename + ".svg"));
        viz.rasterizer(Rasterizer.BATIK).render(Format.PNG).toFile(new File(filename + "b.png"));
        viz.rasterizer(Rasterizer.SALAMANDER).render(Format.PNG).toFile(new File(filename + "s.png"));
    }

    /**
     * Fills a Map with all reachable endstates and the word used
     *
     * @param state       Startzustand
     * @param symbols     Wort
     * @param currentPath das bereits gegangene Wort
     * @param path        return-data. Map von gegangen Wort->State
     */
    public void simulatePathsToEnd(S state, List<A> symbols, List<A> currentPath, Map<List<A>, S> path) {
//        System.out.println("simulatePathsToEnd " + state + symbols.toString());
        if (ends.contains(state))
            path.put(currentPath, state);
        if (symbols.isEmpty()) return;

        List<A> subList = symbols.subList(1, symbols.size());
        List<S> d = this.data.get(state).get(symbols.get(0));

        if (d == null)
            return;
        for (S nextState : d) {
            List<A> p = new ArrayList<>(currentPath);
            p.add(symbols.get(0));
            simulatePathsToEnd(nextState, subList, p, path);
        }
    }

    public S getStart() {
        return start;
    }

    /**
     * Helper method - gets the next Nodes from zustand using Symbol symbol (ignoring any epsilon-edges)
     *
     * @param zustand zustand
     * @param symbol  symbol
     * @return a Collection of the next states
     */
    private Collection<S> getOnlyNext(S zustand, A symbol) {
        Map<A, List<S>> dat = this.data.get(zustand);
        return dat == null || !dat.containsKey(symbol) ? Collections.EMPTY_LIST : dat.get(symbol);
    }

    /**
     * Returns the next States using symbol symbol (including epsion-edges)
     *
     * @param zustand state
     * @param symbol  symbol
     * @return a Set of next nodes
     * @see Automat#getNext(Object, Object, Set)
     */
    public Set<Map.Entry<S, A>> getNext(S zustand, A symbol) {
        Set<Map.Entry<S, A>> ret = new HashSet<>();
        getNext(zustand, symbol, ret);
        return ret;
    }

    private boolean inTransitionList(S zustand, A symbol, Set<Map.Entry<S, A>> transitions) {
        return transitions.stream().anyMatch(e -> e.getKey().equals(zustand) && e.getValue().equals(symbol));
    }

    /**
     * Collects all next states from state zustand using symbol in transitions (including epsilon-edges)
     *
     * @param zustand     the state to go from
     * @param symbol      the symbol being used
     * @param transitions a collection-Set
     */
    public void getNext(S zustand, A symbol, Set<Map.Entry<S, A>> transitions) {
        Map<A, List<S>> next = this.data.get(zustand);
        if (next == null)
            return;
        for (Map.Entry<A, List<S>> e : next.entrySet()) {
            if (e.getKey() == null || e.getKey().equals(symbol)) {
                for (S nextS : e.getValue()) {
                    if (inTransitionList(nextS, e.getKey(), transitions))
                        continue;
                    if (e.getKey() != null || symbol == null)
                        transitions.add(new AbstractMap.SimpleEntry<>(nextS, e.getKey()));
                    if (e.getKey() == null)
                        getNext(nextS, null, transitions);
                }
            }
        }
    }

    /**
     * Returns all symbols defined from state zustand
     *
     * @param zustand state
     * @return immutable Set of next states
     */
    @SuppressWarnings("unchecked")
    private Set<A> getSymbolsFrom(S zustand) {
        Map<A, List<S>> zData = this.data.get(zustand);
        if (zData == null)
            return (Set<A>) Collections.EMPTY_SET;
        return zData.keySet();
    }

    private void addEpsilon(Collection<S> states) {
        boolean newE = false;
        for (S state : new ArrayList<>(states)) {
            Map<A, List<S>> stateData = this.data.get(state);
            if (stateData == null || !stateData.containsKey(null))
                continue;
            for (S s : stateData.get(null)) {
                if (!states.contains(s)) {
                    newE = true;
                    states.add(s);
                }
            }
        }
        if (newE)
            addEpsilon(states);
    }

    /**
     * Potenzmengenkonstruktions Hilffunktion
     *
     * @param NFA             der NFA
     * @param DFA             der DFA (in Konstruktion)
     * @param newState        der State an dem gerade gearbeitet wird
     * @param partsOfNewState die States aus dem NFA, aus denen newState besteht
     * @param <A>             Symboltyp von NFA/DFA
     */
    private static <A> void DFAHelper(Automat<String, A> NFA, Automat<String, A> DFA, String newState, Collection<String> partsOfNewState) {
//      System.out.println("Looking from " + partsOfNewState.toString() + "//" + newState); // - Test-Ausgabe
        NFA.addEpsilon(partsOfNewState); //Alle Ep-Übergänge mit einsammeln
        Set<A> symbols = new HashSet<>(); //no duplicates
        for (String state : partsOfNewState)
            symbols.addAll(NFA.getSymbolsFrom(state));

        //Get rid of any epsilons
        symbols.remove(null);


//        System.out.println("== " + partsOfNewState.toString() + "=" + symbols); // - Test-Ausgabe
        if (partsOfNewState.stream().anyMatch(NFA.ends::contains)) //Ist newState ein Endzustand?
            DFA.addEnd(newState);

        for (A symbol : symbols) { //Für jedes Symbol, dass von newState weggeht
            Set<String> nextStates = new HashSet<>();
            for (String state : partsOfNewState) //Sammle alle nächsten Zustande über symbol
                nextStates.addAll(NFA.getOnlyNext(state, symbol));

//            System.out.println("using " + symbol + " going to " + nextStates.toString()); // Text-Ausgabe

            //Und natürlich die Ep-übergänge hinzufügen
            NFA.addEpsilon(nextStates);

//            System.out.println(" and ep " + nextStates.toString()); // Text-Ausgabe

            //Neuen Namen konstruieren
            String nextStateName = "[" + Joiner.on(',').join(nextStates) + "]";
            if (DFA.addTransition(newState, symbol, nextStateName)) //Und den neuen Zustand mit Übergang hinzufügen
                DFAHelper(NFA, DFA, nextStateName, nextStates); //Falls die Änderung neu ist, das gleiche für den neuen Zustand wiederholen

        }

    }

    /**
     * Umwandlung NFA<String,A> zu einem DFA über die Potenzmengenkonstruktion
     *
     * @return ein DFA von NFA
     */
    @SuppressWarnings("unchecked")
    public Automat<String, A> toDFA() {
        List<java.lang.String> start = new ArrayList<>();

        start.add(this.getStart().toString());
        ((Automat<String, A>) this).addEpsilon(start);


        Automat<String, A> ret = new Automat<>("[" + Joiner.on(",").join(start) + "]");
        DFAHelper((Automat<String, A>) this, ret, ret.getStart(), start);

        return ret;
    }

    /**
     * Dreht einen DFA um
     *
     * @return einen NFA, der die umgedrehte Sprache von diesem Automaten erkennt
     */
    public Automat<String, A> reverse() {
        Automat<String, A> revA = new Automat<>("RSTART" + (this.hashCode() % 20) + (this.hashCode() % 50));
        revA.data.put(revA.getStart(), new HashMap<>());
        for (Map.Entry<S, Map<A, List<S>>> d : this.data.entrySet()) {
            for (Map.Entry<A, List<S>> edges : d.getValue().entrySet()) {
                for (S toNode : edges.getValue())
                    revA.addTransition(toNode.toString(), edges.getKey(), d.getKey().toString());
            }
        }
        for (S endS : this.ends)
            revA.addTransition(revA.getStart(), null, endS.toString());
        revA.addEnd(this.start.toString());
        return revA;
    }

    /**
     * Brzozowski-Minimisierung
     * (Umdrehen -> zum DFA -> umdrehen -> zum DFA)
     * (Verliert leider wichtige Informationen (bzw verschmelz Nodes, siehe Kommentar andere Datei)
     *
     * @return a minimized DFA, welcher die gleiche Sprache wie dieser erkennt.
     */
    public Automat<String, A> minimize() {
        Automat<String, A> reverse = this.reverse().toDFA();

        return reverse.reverse().toDFA();
    }


    //Test-Cases
    public static void main(String[] a) throws Exception {
        Automat<String, Integer> automat = new Automat<>("S1");

        automat.addTransition("S1", 1, "S2");
        automat.addTransition("S1", 2, "S3");

        automat.addTransition("S1", null, "S1e");
        automat.addTransition("S1e", 1, "S2");

        automat.addTransition("S1", 1, "S2b");
        automat.addTransition("S2b", null, "S2");


        automat.addTransition("S2", 1, "S3");

        automat.addTransition("S3", 1, "S4");
        automat.addTransition("S4", -1, "S3");
        automat.addTransition("S3", -1, "S2");
        automat.addEnd("S2");

        automat.toImage("out/addTest");
//
//        automat.toDFA().toImage("out/addTestDFA");
//        automat.reverse().toImage("out/addTestReverse");
        automat.reverse().toDFA().toImage("out/addTestReverseDFA");

        automat.minimize().toImage("out/addTestMin");
    }

}
